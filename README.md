# tl Analytics

Чтобы иметь возможность деплоить на сервер, выполните на локальной машине:

```sh
gem install capistrano --no-doc
gem install capistrano-yarn --no-doc
```

Для непосредственного деплоя:

```sh
yarn run build
git add .
git commit -am 'new build'
git push origin 
cap prodiction deploy
```

## ChangeLog
### v.001
- You can login, and then fetch weeks data from server. Token is storing in localStorage.

### v.002
- Added users matrix visualization page. It shows cohorts of people by target's difference from starting weight and by last signin date's difference from current date.
- Rewrited with redux, router.
- Added log out functionality.

### v.003 
- Improved log in functionality with 'redux-auth-wrapper'
- Calendar inputs in Users page were changed to 'react-datepicker'
