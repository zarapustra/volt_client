# config valid only for current version of Capistrano
# lock '3.4.0'

set :application, 'analytics_tl'
set :repo_url, 'git@bitbucket.org:zarapustra/reactanalytics06.2017.git'
set :user, 'deployer'
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :branch, ENV['BRANCH'] || 'master'
set :app_command, "#{fetch(:deploy_to)}/current/server.production.js --name #{fetch(:application)}"
# set :revision,  ENV['APP_DEPLOYMENT_REVISION']

# Default value for :pty is false
# set :pty, true

# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
# set :linked_files, %w{config/database.yml config/config.yml}
namespace :deploy do

  desc 'Restart application'
  task :restart do
    invoke 'pm2:restart'
  end

  after :publishing, :restart   
end
