import { cloneElement } from 'react';
import { UserAuthWrapper } from 'redux-auth-wrapper';
import { routerActions } from 'react-router-redux';

export const UserIsAuthenticated = UserAuthWrapper({
    authSelector: state => state.auth,
    predicate: auth => auth.isLoggedIn,
    redirectAction: routerActions.replace,
    allowRedirectBack: false
});

export const Authenticated = UserIsAuthenticated(props =>
  cloneElement(props.children, props)
);
