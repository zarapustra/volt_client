import axios from 'axios';

import { baseUrl } from '../config.js';
import { logoutUser } from './authActions';
import ls from 'store';
const token = ls.get('token');

export function saveToStore(name, value) {
    return {
        type: 'USER_SAVE_TO_STORE',
        payload: { name, value }
    };
}

function chooseGroup(users, user, addr1) {
  // по дате последнего входа
    const s = user.sign_in_diff;
    let addr2 = '';
    if (s < 1) {
        addr2 = '0';
    } else if (s <= 3) {
        addr2 = '1';
    } else if (s <= 7) {
        addr2 = '2';
    } else if (s <= 14) {
        addr2 = '3';
    } else {
        addr2 = '4';
    }
    if (users[addr1 + addr2] === undefined) {
        users[addr1 + addr2] = [];
    }
    users[addr1 + addr2].push(user);
}

function sortUsers(rawUsers) {
  // сортируем сначала по target_diff
    const users = {};
    rawUsers.map(user => {
        const t = user.target_diff;
        if (t > 0 && t <= 5) {
            chooseGroup(users, user, 'g0');
        } else if (t > 5 && t <= 10) {
            chooseGroup(users, user, 'g1');
        } else if (t > 10 && t <= 15) {
            chooseGroup(users, user, 'g2');
        } else if (t > 15) {
            chooseGroup(users, user, 'g3');
        }
    });
    return dispatch => {
        dispatch({ type: 'SORT_USERS_FULFILLED', payload: users });
    };
}

// fetch users from server api
function fetchUsers(since, till) {
    const conf = {
        url: baseUrl + 'users',
        headers: {
            Authorization: `Token token=${token}`
        },
        params: {
            since: since,
            till: till
        }
    };
    return dispatch => {
        dispatch({ type: 'FETCH_USERS' });
        return axios.request(conf).then(
      response => {
          dispatch({
              type: 'FETCH_USERS_FULFILLED',
              payload: response.data
          });
      },
      err => {
          if (err.response && err.response.status === 401) {
              dispatch(logoutUser());
          }
          dispatch({ type: 'FETCH_USERS_REJECTED', payload: err });
      }
    );
    };
}

export function fetchAndSortUsers(since, till) {
    return (dispatch, getState) => {
        dispatch(fetchUsers(since, till)).then(
      () => {
          const rawData = getState().users.rawData;
          dispatch(sortUsers(rawData));
      },
      err => {
          dispatch({ type: 'SORT_USERS_REJECTED', payload: err });
      }
    );
    };
}

export function showLinks(address) {
    return { type: 'CHOOSE_GROUP_OF_USERS_TO_SHOW', payload: address };
}
