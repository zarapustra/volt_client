import axios from 'axios';
import { browserHistory } from 'react-router';

import config from '../config.js';
const { baseUrl } = config;
import ls from 'store';

function hash(email, password) {
    return new Buffer(`${email}:${password}`).toString('base64');
}

export function loginUser(email, password) {
    const conf = {
        url: baseUrl + 'login',
        headers: {
            Authorization: `Basic ${hash(email, password)}`
        }
    };
    return dispatch => {
        axios
      .request(conf)
      .then(response => {
          ls.set('token', response.data.token);
          dispatch({
              type: 'LOGIN_USER_FULFILLED'
          });
          browserHistory.push('/');
      })
      .catch(err => {
          dispatch({ type: 'LOGIN_USER_REJECTED', payload: err });
      });
    };
}
export function logoutUser() {
    return dispatch => {
        dispatch({ type: 'LOGOUT_USER_FULFILLED' });
        ls.remove('token');
    };
}
