export default function reducer(
  state = {
      isLoggedIn: false,
      token: null,
      error: null
  },
  action
) {
    switch (action.type) {
        case 'LOGIN_USER_FULFILLED': {
            return {
                ...state,
                isLoggedIn: true
            };
        }
        case 'LOGIN_USER_REJECTED': {
            return {
                ...state,
                error: action.payload
            };
        }
        case 'LOGOUT_USER_FULFILLED': {
            return {
                ...state,
                token: null,
                isLoggedIn: false
            };
        }
        default:
            return state;
    }
}
