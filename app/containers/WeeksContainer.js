import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchWeeks, saveToStore } from '../actions/weekActions';
import WeekTable from '../components/WeekTable';
import styles from '../styles/weeksContainer.scss';

const WeeksContainer = ({
  fetching,
  since,
  till,
  weeks,
  updateStore,
  proceedFetch
}) => {
    let inputSince;
    let inputTill;
    function handleKeyPress(e) {
        if (e.key === 'Enter') {
            proceedFetch(since, till);
        }
    }
    return (
    <div>
      <h1>Weeks</h1>
      <div className={styles.weeksContainer}>
        <input
          type="number"
          placeholder="since"
          value={since}
          ref={node => {
              inputSince = node;
          }}
          onChange={() => updateStore('since', inputSince.value)}
          onKeyPress={handleKeyPress}
        />
        <input
          type="number"
          placeholder="till"
          value={till}
          ref={node => {
              inputTill = node;
          }}
          onChange={() => updateStore('till', inputTill.value)}
          onKeyPress={handleKeyPress}
        />
        <span
          id="load-btn"
          onClick={() => proceedFetch(since, till)}
          className="btn btn-primary btn-md"
        >
          Load <i className="fa fa-sign-in" />
        </span>
        <h2 style={{ display: fetching ? 'block' : 'none' }}>
          Fetching!
        </h2>
      </div>
      <WeekTable weeks={weeks} />
    </div>
  );
};

WeeksContainer.propTypes = {
    fetching: PropTypes.bool,
    since: PropTypes.string,
    till: PropTypes.string,
    weeks: PropTypes.object,
    updateStore: PropTypes.func,
    proceedFetch: PropTypes.func
};

const mapStateToProps = state => {
    const weeks = state.weeks;
    return {
        fetching: weeks.fetching,
        since: weeks.since,
        till: weeks.till,
        weeks: weeks.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateStore: (name, value) => {
            dispatch(saveToStore(name, value));
        },
        proceedFetch: (since, till) => {
            dispatch(fetchWeeks(since, till));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WeeksContainer);
