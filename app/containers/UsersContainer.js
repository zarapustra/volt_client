import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchAndSortUsers, saveToStore } from '../actions/userActions';
import UserTable from '../components/UserTable';
import LinkTable from '../components/LinkTable';
import styles from '../styles/usersContainer.scss';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

const UsersContainer = ({
  fetching,
  since,
  till,
  users,
  chosenGroup,
  updateStore,
  proceedFetch
}) => {
    let group = (users && users[chosenGroup]) || [];
    const handleSince = date => {
        updateStore('since', date);
    };
    const handleTill = date => {
        updateStore('till', date);
    };
    return (
    <div>
      <h1>Users</h1>
      <div className={styles.usersContainer}>
        <DatePicker
          dateFormat="DD.MM.YYYY"
          selected={since}
          onChange={handleSince}
          isClearable
          showMonthDropdown
          showYearDropdown
          dropdownMode="select"
          calendarClassName="react-datepicker-custom"
        />
        <DatePicker
          dateFormat="DD.MM.YYYY"
          selected={till}
          onChange={handleTill}
          isClearable
          showMonthDropdown
          showYearDropdown
          dropdownMode="select"
          calendarClassName="react-datepicker-custom"
        />
        <span
          id="load-btn"
          onClick={() => proceedFetch(since, till)}
          className="btn btn-primary btn-md"
        >
          Load <i className="fa fa-sign-in" />
        </span>
        <h1 style={{ display: fetching ? 'block' : 'none' }}>
          Fetching!
        </h1>
      </div>
      <UserTable users={users} />
      <LinkTable users={group} />
    </div>
  );
};

UsersContainer.propTypes = {
    fetching: PropTypes.bool,
    since: PropTypes.string,
    till: PropTypes.string,
    users: PropTypes.object,
    chosenGroup: PropTypes.string,
    updateStore: PropTypes.func,
    proceedFetch: PropTypes.func
};

const mapStateToProps = state => {
    const users = state.users;
    return {
        fetching: users.fetching,
        since: users.since,
        till: users.till,
        users: users.data,
        chosenGroup: users.chosenGroup
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateStore: (name, value) => {
            dispatch(saveToStore(name, value));
        },
        proceedFetch: (since, till) => {
            dispatch(fetchAndSortUsers(since, till));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersContainer);
