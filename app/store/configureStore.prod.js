import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import promise from 'redux-promise-middleware';
import { browserHistory } from 'react-router';
import { routerMiddleware as initRouterMiddleware} from 'react-router-redux';
const routerMiddleware = initRouterMiddleware(browserHistory);

const middleware = applyMiddleware(promise(), thunkMiddleware, routerMiddleware);

export default function configureStore(initialState) {
    return createStore(rootReducer, initialState, middleware);
}
