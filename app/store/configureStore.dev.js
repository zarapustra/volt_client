import DevTools from '../containers/DevTools';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import promise from 'redux-promise-middleware';
import rootReducer from '../reducers';
import { browserHistory } from 'react-router';
import { routerMiddleware as initRouterMiddleware} from 'react-router-redux';
const routerMiddleware = initRouterMiddleware(browserHistory);
const loggerMiddleware = createLogger();

const enhancer = compose(
  applyMiddleware(promise(), thunkMiddleware, loggerMiddleware, routerMiddleware),
  DevTools.instrument()
);

export default function configureStore(initialState) {
    return createStore(rootReducer, initialState, enhancer);
}
