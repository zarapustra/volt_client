import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { header } from '../styles/header.scss';

const App = ({ children }) => (
  <div>
    <header className={header}>
      <Link to="/">Weeks</Link>
      <Link to="/users">Users</Link>
      <Link to="/logout">Log out</Link>
    </header>
    {children}
  </div>
);

App.propTypes = {
    children: PropTypes.object
};

export default App;
