import React, { Component, PropTypes } from 'react';
import { loginUser } from '../actions/authActions';
import { connect } from 'react-redux';

class Login extends Component {
    state = {
        email: '',
        password: ''
    };

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.proceedLogin(this.state.email, this.state.password);
        }
    }
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        const { error } = this.props;
        return (
      <div>
        <div className="col-md-offset-5 col-md-3">
          <div className="form-login">
            <h4>Welcome back.</h4>
            <input
              type="text"
              id="email"
              name="email"
              onChange={this.handleChange.bind(this)}
              onKeyPress={this.handleKeyPress.bind(this)}
              className="form-control input-sm chat-input"
              placeholder="email"
            />
            <br />
            <input
              type="password"
              id="password"
              name="password"
              className="form-control input-sm chat-input"
              placeholder="password"
              onChange={this.handleChange.bind(this)}
              onKeyPress={this.handleKeyPress.bind(this)}
            />
            <br />
            <div className="wrapper">
              <span className="group-btn">
                <a
                  href="#"
                  id="login-btn"
                  className="btn btn-primary btn-md"
                  onClick={() =>
                    this.props.proceedLogin(
                      this.state.email,
                      this.state.password
                    )}
                >
                  Log in <i className="fa fa-sign-in" />
                </a>
              </span>
              <h3 style={{ display: error ? 'block' : 'none' }}>
                {error && error.message}
              </h3>
            </div>
          </div>
        </div>
      </div>
    );
    }
}
Login.propTypes = {
    proceedLogin: PropTypes.func,
    error: PropTypes.string
};

const mapStateToProps = state => {
    return {
        error: state.auth.error
    };
};
const mapDispatchToProps = dispatch => {
    return {
        proceedLogin: (email, pass) => dispatch(loginUser(email, pass))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
