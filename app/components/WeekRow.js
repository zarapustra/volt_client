import { connect } from 'react-redux';
import React, { Component, PropTypes } from 'react';
import { updateWeek } from '../actions/weekActions';

class WeekRow extends Component {
    state = {
        advCost: this.props.week.adv_cost || 0
    };
    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        const { week } = this.props;
        return (
      <tr>
        <td>{week.number}</td>
        <td>{week.monday}</td>
        <td style={{ textAlign: 'left' }}>
          <div className="btn-group pull-left" role="group">
            <input
              type="number"
              id="advCost"
              name="advCost"
              className="input-sm chat-input"
              value={this.state.advCost}
              style={{ float: 'left', width: '70%' }}
              onChange={this.handleChange.bind(this)}
            />
            <button
              onClick={() => {
                  this.props.proceedUpdate(week.id, this.state.advCost);
              }}
            >
              {">"}
            </button>
          </div>
        </td>
        <td>{week.regs}</td>
        <td>{week.leads}</td>
        <td>{week.revenue} руб.</td>
        <td>{week.revenue_by_cohort} руб.</td>
        <td>{week.cac} руб.</td>
        <td>{week.arppu} руб.</td>
        <td>{week.arppu_cac} руб.</td>
        <td>{week.leads_regs} %</td>
        <td>{week.romi} %</td>
        <td>{week.romi_ltv} %</td>
      </tr>
    );
    }
}

WeekRow.propTypes = {
    proceedUpdate: PropTypes.func,
    week: PropTypes.object
};

const mapStateToProps = state => {
    return state;
};

const mapDispatchToProps = dispatch => {
    return {
        proceedUpdate: (id, advCost) => dispatch(updateWeek(id, advCost))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WeekRow);
