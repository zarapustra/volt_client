import { PropTypes } from 'react';
import { logoutUser } from '../actions/authActions';
import { connect } from 'react-redux';

const Logout = ({ proceedLogout }) => {
    proceedLogout();
    return null;
};

Logout.propTypes = {
    proceedLogout: PropTypes.func
};

const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        proceedLogout: () => dispatch(logoutUser())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
