import React, { PropTypes } from 'react';

const UserLink = ({ user }) => {
    return (
    <div>
      <a href={'https://cloud.10levels.ru/users/' + user.id} style={{cursor: 'pointer', padding: 10}}>
        {'id: ' + user.id + ', target diff: ' + user.target_diff + ', last sign in days ago: ' + user.sign_in_diff}
      </a>
    </div>
  );
};

UserLink.propTypes = {
    user: PropTypes.object
};

export default UserLink;
