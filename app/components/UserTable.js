import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { showLinks } from '../actions/userActions';

const UserTable = ({ users, showInTable }) => {
    return (
    <div>
      <table
        className="table table-condensed table-striped"
        style={{ width: 600 }}
      >
        <thead>
          <tr>
            <th />
            <th>{"< 5 кг"}</th>
            <th>{"5-10 кг"}</th>
            <th>{"10-15 кг"}</th>
            <th>{"> 15 кг"}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>{"был > 14 дней назад"}</th>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g04');
                }}
              >
                {(users.g04 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g14');
                }}
              >
                {(users.g14 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g24');
                }}
              >
                {(users.g24 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g34');
                }}
              >
                {(users.g34 || {}).length || 0}
              </a>
            </td>
          </tr>
          <tr>
            <th>{"был 7-14 дней назад"}</th>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g03');
                }}
              >
                {(users.g03 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g13');
                }}
              >
                {(users.g13 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g23');
                }}
              >
                {(users.g23 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g33');
                }}
              >
                {(users.g33 || {}).length || 0}
              </a>
            </td>
          </tr>
          <tr>
            <th>{"был 3-7 дней назад"}</th>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g02');
                }}
              >
                {(users.g02 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g12');
                }}
              >
                {(users.g12 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g22');
                }}
              >
                {(users.g22 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g32');
                }}
              >
                {(users.g32 || {}).length || 0}
              </a>
            </td>
          </tr>
          <tr>
            <th>{"был 1-3 дней назад"}</th>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g01');
                }}
              >
                {(users.g01 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g11');
                }}
              >
                {(users.g11 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g21');
                }}
              >
                {(users.g21 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g31');
                }}
              >
                {(users.g31 || {}).length || 0}
              </a>
            </td>
          </tr>
          <tr>
            <th>{"был сегодня"}</th>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g00');
                }}
              >
                {(users.g00 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g10');
                }}
              >
                {(users.g10 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g20');
                }}
              >
                {(users.g20 || {}).length || 0}
              </a>
            </td>
            <td>
              <a style={{cursor: 'pointer', padding: 5}}
                onClick={() => {
                    showInTable('g30');
                }}
              >
                {(users.g30 || {}).length || 0}
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

UserTable.propTypes = {
    users: PropTypes.object,
    showInTable: PropTypes.func
};

const mapStateToProps = state => {
    return {
        users: state.users.data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        showInTable: address => {
            dispatch(showLinks(address));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserTable);
