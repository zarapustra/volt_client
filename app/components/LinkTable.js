import React, { PropTypes } from 'react';
import UserLink from './UserLink';

const LinkTable = ({ users }) => {
    let mappedUsers = users.map(user => <UserLink key={user.id} user={user} />);
    return (
    <div>
      {mappedUsers}
    </div>
  );
};

LinkTable.propTypes = {
    users: PropTypes.array
};

export default LinkTable;
