import React, { PropTypes } from 'react';
import WeekRow from './WeekRow';
import { fixedHeaders } from '../styles/weekTable.scss';

const Table = ({ weeks }) => {
    let mappedWeeks = Object.values(weeks).map(week => (
    <WeekRow key={week.id} week={week} />
  ));

    return (
    <div>
      <table className={fixedHeaders}>
        <thead>
          <tr>
            <th>#</th>
            <th>Mondays</th>
            <th>AdvCosts</th>
            <th>Regs</th>
            <th>Leads</th>
            <th>Revenue</th>
            <th>Доход с когорты</th>
            <th>CAC</th>
            <th>ARPPU</th>
            <th>ARPPU/CAC</th>
            <th>Конверсия в платящие</th>
            <th>ROMI</th>
            <th>ROMI(LTV)</th>
          </tr>
        </thead>
        <tbody>
          {mappedWeeks}
        </tbody>
      </table>
    </div>
  );
};

Table.propTypes = {
    weeks: PropTypes.object
};

export default Table;
