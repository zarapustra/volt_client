const path = require('path');
const express = require('express');
const port = 4000;
const app = express();
const indexFile = path.join(__dirname, './dist/index.html');
const publicFolder = express.static(path.join(__dirname, './dist'));

app.use('/', publicFolder);
app.get('/', function(_, res) {
    res.sendFile(indexFile);
});

app.listen(port);
console.log(`Listening at http://localhost:${port}`);
